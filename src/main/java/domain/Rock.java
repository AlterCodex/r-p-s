package domain;

public class Rock extends Figure {
    public boolean winsTo(Figure figure) {
        return figure.isScissors();
    }

        public boolean isRock()
        {
            return true;
        }
}
