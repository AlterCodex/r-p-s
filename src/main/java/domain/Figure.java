package domain;

public abstract class Figure {



    public abstract boolean winsTo(Figure figure);

    public boolean tiesTo(Figure figure) {
        return !figure.winsTo(this) && !winsTo(figure);
    }

    public boolean isRock(){
        return false;
    }

    public boolean isPaper(){
        return false;
    }

    public boolean isScissors(){
        return false;
    }
}
