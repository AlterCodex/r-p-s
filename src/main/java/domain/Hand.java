package domain;

public  class Hand {

    private Figure status;

    public Figure getFigure()
    {
        return status;
    }

    public void setFigure(Figure figure)
    {
        this.status=figure;
    }

    public boolean winsTo(Hand hand){
        return this.status.winsTo(hand.getFigure());

    }


    public boolean tiesTo(Hand hand){
        return this.status.tiesTo(hand.getFigure());

    }


}
