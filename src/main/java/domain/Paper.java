package domain;

public class Paper extends Figure{

    public boolean winsTo(Figure figure) {
        return figure.isRock();
    }

    public boolean isPaper(){
        return true;
    }


}
