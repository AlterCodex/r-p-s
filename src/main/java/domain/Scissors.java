package domain;

public class Scissors extends Figure {

    public boolean winsTo(Figure Figure) {
        return Figure.isPaper();
    }

    public boolean isScissors(){
        return true;
    }
}
